import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        component: () => import('src/layouts/main/MainLayout.vue'),
    },
    {
        path: '/:catchAll(.*)*',
        component: () => import('src/pages/errors/ErrorNotFound.vue'),
    },
];

export default routes;
