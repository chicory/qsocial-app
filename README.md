# QSocial application
Work in progress.

## QSocial Back-End
Go to [qsocial-be repo](https://codeberg.org/chicory/qsocial-be).

## Documentation
Go to [wiki (private repository)](https://codeberg.org/chicory/qsocial-support/wiki).
Public documentation will be added later. 😉

## System requirements

* node.js
* nmp
* yarn
* quasar

## Dev install guide

Clone repo.
```
git clone https://codeberg.org/chicory/qsocial-app.git
cd qsocial-app
```

Install the dependencies.
```
yarn
```

Start the app in development mode (hot-code reloading, error reporting, etc.).
```
quasar dev
```
